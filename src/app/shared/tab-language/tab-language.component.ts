import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-tab-language',
  templateUrl: './tab-language.component.html',
  styleUrls: ['./tab-language.component.css']
})
export class TabLanguageComponent implements OnInit {
  languages: string [] = [];
  activeLanguage: string = 'az';
  activeLangIndex: number = 0;
  constructor() { }

  ngOnInit(): void {
    this.languages = localStorage.getItem('languages').split(',');
  }

  tabChange(event) {
    this.activeLanguage = event.tab.textLabel;
    this.activeLangIndex = event.index;
  }
}
