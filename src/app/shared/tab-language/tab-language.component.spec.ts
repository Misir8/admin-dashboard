import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabLanguageComponent } from './tab-language.component';

describe('TabLanguageComponent', () => {
  let component: TabLanguageComponent;
  let fixture: ComponentFixture<TabLanguageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabLanguageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabLanguageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
