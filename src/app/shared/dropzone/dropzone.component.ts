import {Component, Input, OnInit} from '@angular/core';
import {NgxDropzoneChangeEvent} from 'ngx-dropzone';

@Component({
  selector: 'app-dropzone',
  templateUrl: './dropzone.component.html',
  styleUrls: ['./dropzone.component.css']
})
export class DropzoneComponent implements OnInit {

  files: File[] = [];
  @Input()img: any;
  isSelect: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }

  onSelect(event: NgxDropzoneChangeEvent) {
    this.files.push(...event.addedFiles);
    this.isSelect = false;
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }
}
