import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {MatExpansionModule} from '@angular/material/expansion';
import {TabLanguageComponent} from './tab-language/tab-language.component';
import {RouterModule} from '@angular/router';
import { ImageCropperModule } from 'ngx-image-cropper';
import { DropzoneComponent } from './dropzone/dropzone.component';
import { CropperComponent } from './cropper/cropper.component';

@NgModule({
  declarations: [TabLanguageComponent, DropzoneComponent, CropperComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatBadgeModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
    MatSidenavModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    NgxDropzoneModule,
    MatExpansionModule,
    RouterModule,
    ImageCropperModule,
    MatCardModule
  ],
  exports: [
    MatToolbarModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatBadgeModule,
    MatMenuModule,
    MatButtonModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
    MatSidenavModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    NgxDropzoneModule,
    MatExpansionModule,
    TabLanguageComponent,
    ImageCropperModule,
    DropzoneComponent,
    CropperComponent,
    MatCardModule
  ],
  providers:[]
})
export class SharedModule { }
