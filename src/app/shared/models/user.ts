export interface IUser {
  token: string;
  expires_in: number;
  is_phone_verified: number;
  module_permissions: [];
}

export interface IUserContent {
  content: IUser;
}
