export interface ICountry {
  id: number;
  name: string;
  phone_code: number;
  iso3: string;
  iso: string;
}


