import {Language} from '../../core/language/language';

export interface RootTagObject {
  content: TagContent;
}

export interface TagContent {
  current_page: number;
  data: Tag[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url?: any;
  path: string;
  per_page: string;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Tag {
  id: number;
  tag_type_id: number;
  store_type_id: number;
  media?: Media;
  status: number;
  created_at: string;
  updated_at: string;
  deleted_at?: any;
  applied_branches_count: number;
  tag_type: Tagtype;
  locales: Locale[];
  language: Language
}

export interface Locale {
  title: string;
  locale: string;
}

export interface Tagtype {
  id: number;
  name: string;
}

export interface Media {
  name: string;
  path: string;
}
