import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ImageCroppedEvent, ImageTransform} from 'ngx-image-cropper';
import {NgxDropzoneChangeEvent} from 'ngx-dropzone';

@Component({
  selector: 'app-cropper',
  templateUrl: './cropper.component.html',
  styleUrls: ['./cropper.component.css']
})
export class CropperComponent implements OnInit {

  @Input() file: File;
  canvasRotation = 0;
  showCropper = false;
  containWithinAspectRatio = false;
  transform: ImageTransform = {};
  croppedImage: any = '';

  constructor() { }

  ngOnInit(): void {
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    this.showCropper = true;
  }
}
