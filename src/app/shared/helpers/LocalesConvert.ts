export class LocalesConvert {
  public static localesConvertToLocalesDictionary(data){
    for (let tag of data) {
      tag.locales = tag.locales.reduce((obj, item) => ({...obj, [item.locale]: item}) ,{});
    }
    return data;
  }
}
