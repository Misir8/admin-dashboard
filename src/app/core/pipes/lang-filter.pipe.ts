import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'langFilter',
  pure: false
})
export class LangFilterPipe implements PipeTransform {

  transform(array: any, lang?: string): any {
    return array.filter((x) => x.locale === lang)[0];
  }

}
