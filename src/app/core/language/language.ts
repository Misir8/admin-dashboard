import {Locale} from '../../shared/models/tag';

export class Language {

  private _activeLanguage: string;
  private _dataLocales: Locale[];
  private _activeDataLocales: {};

  public getAttribute(name: string) {
    return this._activeDataLocales[name] ?? null;
  }

  get activeDataLocales(): {} {
    return this._activeDataLocales;
  }

  get dataLocales(): Locale[] {
    return this._dataLocales;
  }

  set dataLocales(value: Locale[]) {
    this._dataLocales = value;
  }

  set activeDataLocales(value: {}) {
    this._activeDataLocales = value;
  }

  get activeLanguage(): string {
    return this._activeLanguage;
  }

  set activeLanguage(lang: string) {
    this._activeLanguage = lang;
    this._activeDataLocales = this._dataLocales.filter(data => data['locale'] == lang)[0];
  }

}
