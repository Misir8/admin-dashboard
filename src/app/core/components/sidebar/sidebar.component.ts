import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {MatExpansionPanel} from '@angular/material/expansion';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Input() isOpen: boolean;
  @Output() openSidebar = new EventEmitter();
  @Output() closeSidebar = new EventEmitter();
  @ViewChildren(MatExpansionPanel) Panels: QueryList<MatExpansionPanel>;

  constructor() { }

  ngOnInit(): void {
  }

  openSidenav(){
    this.openSidebar.emit();
  }
  closeSidenav(){
    for (const panel of this.Panels['_results']) {
      panel.close();
    }
    this.closeSidebar.emit();
  }


}
