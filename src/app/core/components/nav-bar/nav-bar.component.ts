import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Output() onClick = new EventEmitter();
  constructor(private authService: AuthService,
              private router: Router,
              private toast: ToastrService) { }

  ngOnInit(): void {
  }

  logout(){
    this.authService.logout().subscribe(x => {
      localStorage.removeItem('token');
      this.router.navigate(['/login']);
      this.toast.info(x['success']);
    }, error => this.toast.warning(error.error.error));
  }

  sidenavStateChange(){
    this.onClick.emit()
  }

}
