import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {IUserContent} from '../../shared/models/user';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtHelperService = new JwtHelperService();
  url: string = environment.baseUrl;
  constructor(private http: HttpClient) { }


  sendOtpAdmin(value: any){
    console.log(value);
    return this.http.post(this.url + '/v1/send/otp', value);
  }

  login(value: any){
    return this.http.post(this.url + '/v1/login', value)
      .pipe(map((userContent: IUserContent) => {
        if (userContent){
          localStorage.setItem('token', userContent.content.token);
        }
      }));
  }

  loggedIn(): boolean{
    return !this.jwtHelperService.isTokenExpired(localStorage.getItem('token'));
  }

  logout() {
    return this.http.post(this.url + '/v1/logout', {});
  }
}
