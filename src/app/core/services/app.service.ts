import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { formatDate } from '@angular/common';
import {HttpsService} from './https.service';
import * as moment from 'moment'


@Injectable({
  providedIn: 'root'
})
export class AppService extends HttpsService {

  public LOGIN = 'v1/login';
  public LOGIN_OTP = 'v1/send/otp';
  // Tag
  public TAG = 'v1/admin/tag';
  public TAG_TYPE = 'v1/admin/tag/types';
  public TAG_STATUS = 'v1/admin/tag/status';
  public TAG_SHOW = 'v1/admin/tag/show';
  // Catalog
  public CATALOG = 'v1/admin/catalog';
  public CATALOG_STATUS = 'v1/admin/catalog/status';
  // Store
  public STORE = 'v1/admin/store';
  public STORE_TYPE = 'v1/admin/store/types';
  public STORE_STATUS = 'v1/admin/store/status';
  public STORE_WITH_BRANCHES = 'v1/admin/store/with/branches';
  // Product
  public PRODUCT = 'v1/admin/product';
  public PRODUCT_STATUS = 'v1/admin/product/status';
  public PRODUCT_DISABLE = 'v1/admin/product/disable';
  public PRODUCT_STOCK = 'v1/admin/product/stock/product';
  public PRODUCT_VARIANT = 'v1/admin/product/variants';


  // Country
  public COUNTRY = 'v1/countries';
  public CURRENCY = 'v1/currencies';
  public CITY = 'v1/cities';

  // User
  public USER = 'v1/admin/user/management';
  public USER_STATUS = 'v1/admin/user/management/status';
  public USER_ROLE = 'v1/admin/user/management/list/roles';
  // Role
  public ROLE = 'v1/admin/permission/management/roles';
  public ROLE_SHOW = 'v1/admin/permission/management/role';
  public ROLE_STATUS = 'v1/admin/permission/management/roles/status';
  public MODULES = 'v1/admin/permission/management/modules';
  public CREATE_ROLE = 'v1/admin/permission/management/set';
  // Branch
  public BRANCH = 'v1/admin/branch';
  public BRANCH_STATUS = 'v1/admin/branch/status';

  // Collection
  public COLLECTION = 'v1/admin/branch/collection/management';
  public COLLECTION_STATUS = 'v1/admin/branch/collection/management/status';
  public COLLECTION_PULISH = 'v1/admin/branch/collection/management/publish';
  public COLLECTION_UNPULISH = 'v1/admin/branch/collection/management/unPublish';
  public PRODUCT_WITH_COMBINATIONS = 'v1/admin/product/with/combinations';



  constructor(public http: HttpClient) {
    super();
  }

  /**
   * GET methods
   */

  public getList(params: any = {}, route_url): Observable<any> {
    return this.get(this.http, route_url, params);
  }

  public getItem(params: any = {}, route_url): Observable<any> {
    return this.get(this.http, `${route_url}/${params.id}`, params);
  }


  /**
   * POST/PUT methods
   */

  public postdata(params: any = {}, route): Observable<any> {

    this.cleanObject(params);
    params = this.formatDate(params);
    if (params.id) {
      return this.put(this.http, `${route}/${params.id}`, params);
    } else {
      return this.post(this.http, route, params);
    }
  }

  public postFormData(params: any = {}, file, fileName, route): Observable<any> {
    this.cleanObject(params);
    params = this.formatDate(params);
    const formData = new FormData();

    Object.entries(params).forEach(element => {
      // TODO: optimize there
      if (element[0] == 'locales') {
        formData.append('locales[]', params.locales);
      } else {
        formData.append(element[0], String(element[1]));
      }
    });
    if (file) {
      formData.append(fileName, file);

    }
    if (params.id) {
      return this.post(this.http, `${route}/${params.id}`, formData);
    } else {
      return this.post(this.http, route, formData);
    }
  }

  /**
   * DELETE Method
   */
  public deleteData(params: any = {}, route): Observable<any> {
    return this.delete(this.http, `${route}/${params.id}`, params);
  }


  /**
   * Helpers
   */
  public formatDate(params) {
    Object.entries(params).forEach(element => {
      // trick for send empty date as value
      if (element[1] == '00.00.00') {
        params[element[0]] = '';
      }
      if ((element[1] instanceof Date) || element[1] instanceof moment) {
        params[element[0]] = formatDate(String(element[1]), 'yyyy-MM-dd', 'en');
      }

    });
    return params;
  }

  public trackByFn(index, item) {
    return index; // or item.id
  }

  cleanObject(obj) {
    // tslint:disable-next-line: forin
    for (const propName in obj) {
      if (
        obj[propName] === 'null' ||
        obj[propName] === null ||
        obj[propName] === undefined ||
        obj[propName] === []
      ) {
        delete obj[propName];
      }
      if (obj[propName] == 'false') {
        obj[propName] = false;
      }
    }
  }

  cleanFormData(formData: any) {
    for (let pair of formData.entries()) {
      if (pair[1] == 'undefined' || pair[1] == undefined || pair[1] == null) {
        formData.delete(pair[0]);
      }
    }
    return formData;
  }

  public postdataCollection(params: any = {}, route): Observable<any> {

    this.cleanObject(params);
    params = this.formatDate(params);
    if (params.id) {
      return this.put(this.http, `${route}/${params.id}?pretend_branch_id=${params.pretend_branch_id}`, params);
    } else {
      return this.post(this.http, `${route}?pretend_branch_id=${params.pretend_branch_id}`, params);
    }
  }

}
