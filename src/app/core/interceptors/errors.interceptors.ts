import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';


@Injectable()
export class ErrorsInterceptors implements HttpInterceptor{


  constructor(private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(err => {
        if (err) {
          if (err.status === 404) {
            this.router.navigateByUrl('/login');
            console.log('not found error', err);
          }
          if (err.status === 500) {
            console.log('server side error', err);
          }
          if (err.status === 400) {
            console.log('validation error', err);
          }
        }
        return throwError(err);
      })
    );
  }
}
