import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginLayoutComponent} from './components/login-layout/login-layout.component';
import {MainLayoutComponent} from './components/main-layout/main-layout.component';
import {NavBarComponent} from './components/nav-bar/nav-bar.component';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {LangFilterPipe} from './pipes/lang-filter.pipe';


@NgModule({
  declarations: [
    LoginLayoutComponent,
    MainLayoutComponent,
    NavBarComponent,
    SidebarComponent,
    LangFilterPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule
  ],
  exports: [
    LoginLayoutComponent,
    MainLayoutComponent,
    NavBarComponent,
    RouterModule,
    SidebarComponent,
    LangFilterPipe
  ]
})
export class CoreModule {
}
