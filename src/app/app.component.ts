import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  languages: string [] = ['az', 'en', 'ru', 'tr'];
  ngOnInit(): void {
    localStorage.setItem('languages', this.languages.toString());
  }
}
