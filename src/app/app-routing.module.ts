import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './core/guards/auth.guard';
import {LoginLayoutComponent} from './core/components/login-layout/login-layout.component';
import {MainLayoutComponent} from './core/components/main-layout/main-layout.component';
import {HomeComponent} from './pages/home/home.component';
import {ExitGuard} from './core/guards/exit.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', canActivate:[ExitGuard], component: LoginLayoutComponent, pathMatch: 'full', children: [
      { path: '', loadChildren: () => import('./pages/login/modules/login.module').then(m => m.LoginModule) },
    ]},
  { path: 'admin', component: MainLayoutComponent, canActivate: [AuthGuard],  children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'tag', loadChildren: () => import('./pages/tag/modules/tag.module').then(m => m.TagModule), canActivate: [AuthGuard] },
    ]},

  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
