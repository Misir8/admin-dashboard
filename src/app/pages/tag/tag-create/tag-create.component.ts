import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {TagService} from '../services/tag.service';
import {Locale, Tag, Tagtype} from '../../../shared/models/tag';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CropperComponent} from '../../../shared/cropper/cropper.component';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {ErrorStateMatcher} from '../../../shared/helpers/ErrorStateMatcher';
import {AppService} from '../../../core/services/app.service';

@Component({
  selector: 'app-tag-create',
  templateUrl: './tag-create.component.html',
  styleUrls: ['./tag-create.component.css']
})
export class TagCreateComponent implements OnInit {

  get names() {
    return this.tagCreateForm.get('names') as FormArray;
  }

  get type() {
    return this.tagCreateForm.get('tagType');
  }

  tagCreateForm: FormGroup;
  tagTypes: Tagtype[];
  isOneChange: boolean = false;
  languages: string[] = [];
  tagId: number;
  tag: Tag;
  imgUrl = environment.imageUrl;
  matcher = new ErrorStateMatcher();

  constructor(private appService: AppService,
              private fb: FormBuilder,
              private router: Router,
              private elem: ElementRef,
              private activatedRoute: ActivatedRoute,
              private toast: ToastrService) {
  }

  @ViewChild('cropper', {static: false}) cropper: CropperComponent;

  ngOnInit(): void {
    this.languages = localStorage.getItem('languages').split(',');
    this.getTagTypes();
    this.formLoad();
    this.tagId = this.activatedRoute.snapshot.params['id'];
    this.getTagById();
  }

  getTagById() {
    if (this.tagId !== undefined) {
      this.appService.getItem({id: this.tagId}, this.appService.TAG_SHOW)
        .subscribe(x => {
          this.tag = x['content'];
          this.patchFormLoad();
        });
    }
  }

  getTagTypes() {
    this.appService.getList({}, this.appService.TAG_TYPE).subscribe(x => {
      this.tagTypes = x['content'];
    }, error => console.log(error));
  }

  updateTag() {
    let tag = this.createTag(this.fillLocales());
    this.updateTagRequest(tag);
  }

  formLoad() {
    this.tagCreateForm = this.fb.group({
      file: [''],
      names: this.fb.array([]),
      tagType: ['', Validators.required]
    });
    for (let language of this.languages) {
      this.names.push(this.fb.control('', Validators.required));
    }
  }

  patchFormLoad() {
    this.tagCreateForm.patchValue({
      file: [''],
      tagType: [this.tag.tag_type_id, Validators.required]
    });
    for (let i = 0; i < this.names.controls.length; i++) {
      this.names.controls[i].patchValue(this.tag.locales[i].title);
    }
  }


  nameChange(index: number) {
    if (this.isOneChange || (this.tagId != undefined)) {
      return;
    }
    let value: string[] = [];
    for (let i = 0; i < this.languages.length; i++) {
      value.push(this.names.value[index]);
    }
    this.names.setValue(value);
    this.isOneChange = true;
  }

  private fillLocales() {
    let locales: Locale[] = [];
    for (let i = 0; i < this.names.value.length; i++) {
      locales.push({locale: this.languages[i], title: this.names.value[i]});
    }
    return locales;
  }

  private createTag(locales: Locale[]) {
    let tag = {
      tag_type_id: this.tagCreateForm.get('tagType').value,
      store_type_id: 1,
      locales: locales,
      media: this.cropper.croppedImage === '' ? '' : this.cropper.croppedImage
    };
    if (this.tagId && this.cropper.croppedImage === '') {
      delete tag.media;
    }
    return tag;
  }

  private updateTagRequest(tag: any) {
    if(this.tagId){
      tag.id = this.tagId;
    }
    this.appService.postdata(tag, this.appService.TAG).subscribe(x => {
      this.router.navigate(['/admin/tag']);
      this.toast.success('Tag success edited');
    }, error => this.toast.error(error.error.error));
  }
}
