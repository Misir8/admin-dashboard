import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TagListComponent} from '../tag-list/tag-list.component';
import {TagCreateComponent} from '../tag-create/tag-create.component';

const routes: Routes = [
  { path: '', component: TagListComponent },
  { path: 'create', component: TagCreateComponent },
  { path: 'edit/:id', component: TagCreateComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TagRoutingModule { }
