import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagListComponent } from '../tag-list/tag-list.component';
import {SharedModule} from '../../../shared/shared.module';
import {TagRoutingModule} from './tag-routing.module';
import {CoreModule} from '../../../core/core.module';
import { TagDeleteDialogComponent } from '../tag-delete-dialog/tag-delete-dialog.component';
import { TagCreateComponent } from '../tag-create/tag-create.component';
import {ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [TagListComponent, TagDeleteDialogComponent, TagCreateComponent],
  imports: [
    CommonModule,
    SharedModule,
    TagRoutingModule,
    CoreModule,
    ReactiveFormsModule,
  ]
})
export class TagModule { }
