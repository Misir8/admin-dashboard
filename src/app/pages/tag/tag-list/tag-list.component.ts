import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Tag} from '../../../shared/models/tag';
import {MatDialog} from '@angular/material/dialog';
import {TagDeleteDialogComponent} from '../tag-delete-dialog/tag-delete-dialog.component';
import {environment} from '../../../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../../core/services/app.service';
import {map} from 'rxjs/operators';
import {LocalesConvert} from '../../../shared/helpers/LocalesConvert';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {
  dataSource;
  response: any;
  displayedColumns: string[] = ['id', 'title', 'tagType', 'storeCount', 'status', 'action'];
  imageUrl = environment.imageUrl;
  params = {
    tag_type: 1,
    per_page: 6
  };

  constructor(private appService: AppService,
              public dialog: MatDialog,
              private elem: ElementRef,
              private toast: ToastrService,) {
  }

  ngOnInit(): void {
    this.getTags();
  }

  getTags(size?: number) {
    if (size != null) {
      this.params.per_page += this.params.per_page;
    }
    this.appService.getList({tag_type: 1, per_page: this.params.per_page}, this.appService.TAG)
      .subscribe(response => {
        response.content.data = LocalesConvert.localesConvertToLocalesDictionary(response.content.data);
        console.log(response.content.data);
        this.response = response;
        this.dataSource = response.content.data;
      });
  }

  openDeleteDialog(element: Tag) {
    let data = {getTags: () => this.getTags(), element: element};
    this.dialog.open(TagDeleteDialogComponent, {
      data: data,
      minWidth: '40vw',
    });
  }

  statusChange(id: number, status: number) {
    status = status === 1 ? 0 : 1;
    this.appService.postdata({status: status, id: id}, this.appService.TAG_STATUS).subscribe(x => {
      this.getTags();
      status === 1 ? this.toast.success(x['success'] + '. Status activated') :
        this.toast.info(x['success'] + '. Status deactivated');
    }, error => this.toast.error(error.error.error));
  }
}
