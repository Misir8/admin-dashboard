import {Component, Inject, Input, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {TagService} from '../services/tag.service';
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../../core/services/app.service';

@Component({
  selector: 'app-tag-delete-dialog',
  templateUrl: './tag-delete-dialog.component.html',
  styleUrls: ['./tag-delete-dialog.component.css']
})
export class TagDeleteDialogComponent implements OnInit {

  @Input() id: number;
  constructor(private appService: AppService,
              private toast: ToastrService,
              @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
  }


  onDeleteTag() {
    this.appService.deleteData({id:this.data.element.id}, this.appService.TAG).subscribe(x =>{
      this.toast.success(x['success']);
      this.data.getTags();
    },error => this.toast.error(error.error));
  }
}
