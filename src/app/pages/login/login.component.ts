import { Component, OnInit } from '@angular/core';
import {Subscription, timer} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ICountry} from '../../shared/models/country';
import {AuthService} from '../../core/services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AppService} from '../../core/services/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  resendCount: number = 0;
  countDown: Subscription;
  counter: number = 10;
  isRemoveCounter: boolean;
  isSuccessLogin: boolean;
  tick: number = 1000;
  isVerification: boolean;
  adminPhoneNumber: any;
  adminName: any;
  form: FormGroup;
  securityForm: FormGroup;
  countries: ICountry[];
  phoneCode: any;

  constructor(private authService: AuthService,
              private appService: AppService,
              private fb: FormBuilder,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    this.phoneFormsValidation();
    this.getCountries();
    this.securityForm = this.fb.group({
      otp: [null, Validators.required]
    })
  }

  phoneFormsValidation(){
    this.form = this.fb.group({
      phone: [null, [Validators.required]],
      country_id: [1, [Validators.required]]
    })
  }

  getCountries(){
    this.appService.getList({}, this.appService.COUNTRY).subscribe( content=> {
      this.countries = content['content'] as ICountry[];
    }, error => console.log(error));
  }

  sendPhoneNumber() {
    if (this.form.valid){
      let value = {phone: +this._phone.value, country_id: +this._country_id.value};
      this.authService.sendOtpAdmin(value).subscribe(resp=> {
        this.phoneCode = this.countries.filter(x => x.id === value.country_id)[0].phone_code;
        this.sendPhoneNumberResponseSuccess(resp);
        this.countDownTimer();
      }, error => {
        this.toastr.error(error.error.error);
      });
    }
  }


  sendSecurityCodeLogin() {
    if (this.securityForm.valid){
      let value = {
        country_id: this._country_id.value,
        phone: this._phone.value,
        otp: +this.securityForm.get('otp').value
      };
      this.authService.login(value).subscribe(x => {
        this.isSuccessLogin = true;
        this.toastr.success("Success Log in");
        this.router.navigate(['/admin/home']);
      }, error => this.toastr.error(error.error.error))
    }

  }

  countDownTimer(){
    if(this.resendCount < 4){
      this.resendCount ++;
      this.isRemoveCounter = false;
      this.counter = 10;
      this.tick = 1000;
      this.timerEvent();
      if (this.resendCount > 1){
        let value = {phone: +this._phone.value, country_id: +this._country_id.value};
        this.authService.sendOtpAdmin(value).subscribe(resp=> {
          this.sendPhoneNumberResponseSuccess(resp);
        }, error => {
          this.toastr.error(error.error.error);
        });
      }
    }else{
      this.toastr.warning('You\'ve used up all the attempts ' + (this.resendCount -1).toString());
    }
  }

  private sendPhoneNumberResponseSuccess(resp){
    // @ts-ignore
    this.adminName = resp.content.name;
    let countryCode: number = this.countries.filter((x:ICountry) =>
      x.id == +this._country_id.value)[0].phone_code;
    this.adminPhoneNumber = countryCode + this._phone.value;
    this.isVerification = true;
  }

  private timerEvent() {
    this.countDown = timer(0, this.tick).subscribe(() => {
      --this.counter
      if(this.isSuccessLogin){
        this.countDown.unsubscribe();
      }
      if(this.counter == 0){
        if(this.countDown.unsubscribe != null){
          this.countDown.unsubscribe();
          this.isRemoveCounter = true;
        }
      }
    });
  }

  get _phone(){
    return this.form.get('phone');
  }
  get _country_id(){
    return this.form.get('country_id')
  }


}
