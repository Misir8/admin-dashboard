import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ToastrModule} from 'ngx-toastr';
import { HomeComponent } from './pages/home/home.component';
import {JwtModule} from '@auth0/angular-jwt';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {HeadersInterceptors} from './core/interceptors/headers.interceptors';
import {ErrorsInterceptors} from './core/interceptors/errors.interceptors';

export function tokenGetter() {
  return localStorage.getItem("token");
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CoreModule,
    ToastrModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['https://dev-api.fooderos.com/api/'],
        disallowedRoutes: ['https://dev-api.fooderos.com/api/v1/login/'],
      }
    }),
    SharedModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptors, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptors, multi: true },
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
